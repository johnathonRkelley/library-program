#import the library used to query a website
import urllib.request #if you are using python3+ version, import urllib.request
import sys

if len(sys.argv) == 1:
    print("Please add two arguments")
else: 
    isbn = sys.argv[1] #Gets first variable of args
    #isbn = "0606264477"
    #isbn = "9780545010221"
    #specify the url
    wiki = "https://www.bookfinder.com/search/?author=&title=&lang=en&isbn=" + str(isbn) + "&new_used=*&destination=us&currency=USD&mode=basic&st=sr&ac=qr"
    #Query the website and return the html to the variable 'page'
    page = urllib.request.urlopen(wiki) 
    #import the Beautiful soup functions to parse the data returned from the website
    from bs4 import BeautifulSoup
    #Parse the html in the 'page' variable, and store it in Beautiful Soup format
    soup = BeautifulSoup(page)
    all_classes = soup.find_all("span", attrs={'class': 'results-price'})
    print(all_classes[0].string)