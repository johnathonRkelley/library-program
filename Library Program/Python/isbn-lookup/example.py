#!/usr/bin/env python
#example url https://pypi.org/project/isbntools/
#https://isbnlib.readthedocs.io/en/latest/devs.html#api-s-main-namespaces
#https://github.com/xlcnd/isbntools


import sys
from isbntools.app import *

f = open("data.txt", 'w')
query = sys.argv[1].replace(' ', '+')
isbn = isbn_from_words(query)

print("The ISBN of the most `spoken-about` book with this title is %s" % isbn)
print("")
print("... and the book is:")
print("")
print(isbn)
print(registry.bibformatters['labels'](meta(isbn)))
print(registry.bibformatters['endnote'](meta(isbn)))
f.write(registry.bibformatters['endnote'](meta(isbn)))
f.close()