﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library_Program
{
    public class Book
    {
        public String bookName;
        public String bookAuthor;
        public String isbn;
        private String[] bookInformation;
        private double price;
        private int id;

        public Book()
        {

        }

        public Book(String name, String author, String number)
        {
            bookName = name;
            bookAuthor = author;
            isbn = number;
        }

        public Book(int index, String name, String author, String number)
        {
            bookName = name;
            bookAuthor = author;
            isbn = number;
            id = index;

            bookInformation = new String[4];
            bookInformation[0] = name;
            bookInformation[1] = author;
            bookInformation[2] = number;
        }

        public Book(int index, String[] data)
        {
            id = index;
            bookName = data[0];
            bookAuthor = data[1];
            isbn = data[2];
            bookInformation = data;
        }

        public String[] getBookInformation()
        {
            return bookInformation;
        }

        public String savedData()
        {
            return id + "/" + BookName + "/" + BookAuthor + "/" + Isbn;
        }

        public String BookName
        {
            get { return bookName; }
            set { bookName = value; }
        }

        public String Isbn
        {
            get { return isbn; }
            set { isbn = value; }
        }

        public String BookAuthor
        {
            get { return bookAuthor; }
            set { bookAuthor = value; }
        }

        public int getId()
        {
            return id;
        }
    }

}