﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Library_Program
{
    public partial class frmAddBook : Form
    {
        private String CACHE_FILE;
        private List<Book> books;

        /* Constructors */ 
        public frmAddBook()
        {
            CACHE_FILE = System.IO.Directory.GetCurrentDirectory() + @"\example.txt";
            InitializeComponent();

            books = new List<Book>();

            //Initalizes and Clears Widgets being used.
            txtAuthor.Text = "";
            txtBookName.Text = "";
            txtISBN.Text = "";
            btnSubmit.Text = "Submit";
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            PythonHandler p = new PythonHandler();
            //p.run_cmd("example.py" , "");
        }     
        /* Button Clicks */
        private void btnSubmit_Click(object sender, EventArgs e)
        {
            //If one text box is blank, don't allow submit.
            if (txtISBN.Text == "" || txtAuthor.Text == "" || txtBookName.Text == "")
            {
                Console.WriteLine("Make sure all data is inputted");
            }
            else 
            {
                //Database d = new Database();
                //d.Insert(txtBookName.Text, txtAuthor.Text, txtISBN.Text);
                //books.Add(new Book(txtBookName.Text, txtAuthor.Text, txtISBN.Text));
                //String[] lines = { txtBookName.Text, txtAuthor.Text, txtISBN.Text };
                //System.IO.File.WriteAllLines(CACHE_FILE, lines); 

                FirebaseDB db = new FirebaseDB();
                
                var book = new Book3{
                    bookName = txtBookName.Text,
                    bookAuthor = txtAuthor.Text,
                    isbn = txtISBN.Text
                };

                db.insertData(book);
            }

            this.Close();
        }
 
    }
}
