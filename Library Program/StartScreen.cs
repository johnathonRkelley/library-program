﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Library_Program
{
    public partial class StartScreen : Form
    {
        private int seconds;
        
        public StartScreen()
        {
            seconds = 0;
            InitializeComponent();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            if (seconds == 5)
            {
                MainScreen frm = new MainScreen();

                frm.Show();
                timer.Enabled = false;
                this.Hide();
            }
            else
            {
                seconds = 1 + seconds;
            }
        }
    }
}
