﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Library_Program
{
    public partial class frmIsbnLookup : Form
    {
        private String CACHE_FILE;

        /* Constructor */
        public frmIsbnLookup()
        {
            InitializeComponent();
        }
        private void frmIsbnLookup_Load(object sender, EventArgs e)
        {
            btnLookup.Text = "Look Up";
            txtISBN.Text = "";
            txtResult.Text = "";

            CACHE_FILE = System.IO.Directory.GetCurrentDirectory() + @"\example.txt";
        }

        /* Button Clicks */
        private void btnLookup_Click(object sender, EventArgs e)
        {
            PythonHandler p = new PythonHandler();
            String isbn = txtISBN.Text;
            Console.WriteLine("ISBN : " + isbn);
            if (isbn == null)
            {
                Console.WriteLine("Please input data into the ISBN field");
            }
            else
            {
                /*  
                   Example Result for data
                   %0 Book
                   %T Harry Potter And The Philosopher's Stone
                   %A J. K. Rowling
                   %@ 9780747532699
                   %D 1997
                   %I Bloomsbury Pub Limited
                */
                
                List<String> data = p.run_cmd(isbn); // Looks up ISBN

                for (int i = 0; i < data.Count; i++)
                {
                    txtResult.AppendText(data[i] + Environment.NewLine);
                }

                String[] lines = {data[1].Substring(2), data[2].Substring(2), txtISBN.Text};
                System.IO.File.WriteAllLines(CACHE_FILE, lines);

                //Database d = new Database();
                //Console.WriteLine(data[1].Substring(3) + data[2].Substring(3));
                //d.Insert(data[1].Substring(3), data[2].Substring(3), txtISBN.Text);

                FirebaseDB db = new FirebaseDB();

                var book = new Book3
                {
                    bookName = data[1].Substring(3),
                    bookAuthor = data[2].Substring(3),
                    isbn = txtISBN.Text

                };

                db.insertData(book);
            }

            
        }
    }
}
