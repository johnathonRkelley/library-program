﻿using FireSharp.Config;
using FireSharp.Interfaces;
using FireSharp.Response;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Library_Program
{
    class FirebaseDB
    {
        List<String> list;
        EventStreamResponse response;

        IFirebaseConfig config = new FirebaseConfig
        {
            AuthSecret = "1Qi9JVXTNkHanRcxx6FXobo3EEFO57AANhnZ6Oaw",
            BasePath = "https://testing-database-fe3cb.firebaseio.com/"
        };

        IFirebaseClient client;

        public FirebaseDB()
        {
            client = new FireSharp.FirebaseClient(config);
        }

        public void insertData(Book3 book)
        {
            String path = "books";
            client.Push(path, book);
        }

        public List<Book3> getData()
        {
            FirebaseResponse response = client.Get("/books/");
            string JsTxt = response.Body;  
            dynamic data = JsonConvert.DeserializeObject<dynamic>(JsTxt);

            var list = new List<Book3>();
            foreach (var itemDynamic in data)
            {
                list.Add(JsonConvert.DeserializeObject<Book3>
                (((JProperty)itemDynamic).Value.ToString()));
            }

            
            Console.WriteLine("We dem bois");

            return list;
        }
    }

    public class Value
    {
        [JsonProperty(PropertyName = "bookName")]
        public String bookName { get; set; }
        [JsonProperty(PropertyName = "bookAuthor")]
        public String bookAuthor { get; set; }
        [JsonProperty(PropertyName = "isbn")]
        public String isbn { get; set; }
    }
}
