﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Add MySql Library
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Windows.Forms;

namespace Library_Program
{
    class Database
    {
        private MySqlConnection connection;
        private string server;
        private string database;
        private string uid;
        private string password;

        //Constructor
        public Database()
        {
            Initialize();
        }

        //Initialize values
        private void Initialize()
        {
            server = "localhost";
            database = "bookdatabase";
            uid = "johnathon";
            password = "";
            string connectionString;
            connectionString = "SERVER=" + server + ";" + "DATABASE=" +
            database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";

            connection = new MySqlConnection(connectionString);
        }

        //open connection to database
        private bool OpenConnection()
        {
            try
            {
                connection.Open();
                return true;
            }
            catch (MySqlException ex)
            {
                //When handling errors, you can your application's response based 
                //on the error number.
                //The two most common error numbers when connecting are as follows:
                //0: Cannot connect to server.
                //1045: Invalid user name and/or password.
                switch (ex.Number)
                {
                    case 0:
                        Console.WriteLine("Cannot connect to server.  Contact administrator");
                        break;

                    case 1045:
                        Console.WriteLine("Invalid username/password, please try again");
                        break;
                }
                return false;
            }
        }

        //Close connection
        private  bool CloseConnection()
        {
            try
            {
                connection.Close();
                return true;
            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex.ToString());
                return false;
            }
        }

        //Insert statement
        public void Insert(String book, String author, String isbn)
        {

            string query = "INSERT INTO books (name, author, isbn) VALUES(";

            book = book.Replace("'", "");
            author = author.Replace("'", "");

            //Input values
            query += "'" + book + "',";
            query += "'" + author + "',";
            query += "'" + isbn + "');";

            //open connection
            if (this.OpenConnection() == true)
            {
                //create command and assign the query and connection from the constructor
                MySqlCommand cmd = new MySqlCommand(query, connection);

                //Execute command
                cmd.ExecuteNonQuery();

                //close connection
                this.CloseConnection();
            }
        }

        //Update statement
        public void Update()
        {
            string query = "UPDATE tableinfo SET name='Joe', age='22' WHERE name='John Smith'";

            //Open connection
            if (this.OpenConnection() == true)
            {
                //create mysql command
                MySqlCommand cmd = new MySqlCommand();
                //Assign the query using CommandText
                cmd.CommandText = query;
                //Assign the connection using Connection
                cmd.Connection = connection;

                //Execute query
                cmd.ExecuteNonQuery();

                //close connection
                this.CloseConnection();
            }
        }

        //Update statement
        public void Update(int id, String bookName, String authorName, String isbn)
        {
            string query = "UPDATE books SET name=";
            query += "'" + bookName +"', author=";
            query +="'" + authorName + "', isbn=";
            query += "'" + isbn + "' WHERE id=";
            query += "'" + id + "';";

            //Open connection
            if (this.OpenConnection() == true)
            {
                //create mysql command
                MySqlCommand cmd = new MySqlCommand();
                //Assign the query using CommandText
                cmd.CommandText = query;
                //Assign the connection using Connection
                cmd.Connection = connection;

                //Execute query
                cmd.ExecuteNonQuery();

                //close connection
                this.CloseConnection();
            }
        }
        
        //Delete statement
        public void Delete(int id)
        {
            string query = "DELETE FROM books WHERE id='";
            query += id + "';";

            MessageBoxButtons buttons = MessageBoxButtons.YesNo;

            if (this.OpenConnection() == true)
            {
                DialogResult result = MessageBox.Show("Would you want to delete this?", "Delete the selected data?", buttons);

                if (result == System.Windows.Forms.DialogResult.Yes)
                {
                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    cmd.ExecuteNonQuery();
                    this.CloseConnection();
                }
            }
        }

        //Select statement
        public List<Book> Select()
        {
            string query = "SELECT * FROM books";
            Console.WriteLine("Selecting from books ");
            //Create a list to store the result
            List<Book> books = new List<Book>();

            
            //Open connection
            if (this.OpenConnection() == true)
            {
                //Create Command
                MySqlCommand cmd = new MySqlCommand(query, connection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    String data = dataReader["id"] + "/" + dataReader["name"] + "/" + dataReader["author"] + "/" + dataReader["isbn"];
                    Console.WriteLine(data);
                    Backend backend = new Backend();
                    books.Add(backend.createBook(backend.parsedData(data)));
                }

                //close Data Reader
                dataReader.Close();

                //close Connection
                this.CloseConnection();

                //return list to be displayed
                return books;
            }
            else
            {
                return books;
            }
        }

        public int GetIndex(String bookName)
        {
            string query = "select id from books where name = '";
            query += bookName + "';";

            int index = 0;

            //Open connection
            if (this.OpenConnection() == true)
            {
                //Create Command
                MySqlCommand cmd = new MySqlCommand(query, connection);
                //Create a data reader and Execute the command
                index = (Int32) cmd.ExecuteScalar();
                
                Console.WriteLine("Found " + bookName + " at index of " + index + ". ");
                //close Data Reader

                //close Connection
                this.CloseConnection();

                return index;
            }
            else
            {
                return -1;
            }
        }

        //Count statement
        public int Count()
        {
            string query = "SELECT Count(*) FROM tableinfo";
            int Count = -1;

            //Open Connection
            if (this.OpenConnection() == true)
            {
                //Create Mysql Command
                MySqlCommand cmd = new MySqlCommand(query, connection);

                //ExecuteScalar will return one value
                Count = int.Parse(cmd.ExecuteScalar() + "");

                //close Connection
                this.CloseConnection();

                return Count;
            }
            else
            {
                return Count;
            }
        }

        //Backup
        public void Backup()
        {
        }

        //Restore
        public void Restore()
        {
        }
    }
}
