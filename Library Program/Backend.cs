﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Library_Program
{
    class Backend
    {
        private String fileName;

        /* Constructor */
        public Backend()
        {
            fileName = System.IO.Directory.GetCurrentDirectory() + @"\saved.txt";

        }

        /* Getter / Setters */
        public void saveData(Book[] books)
        {
            String[] example = { "2/ Jim Cramer's Real Money/ Jim Cramer/0743224892", "2/ Jim Cramer's Real Money/ Jim Cramer/0743224892", "2/ Jim Cramer's Real Money/ Jim Cramer/0743224892", "2/ Jim Cramer's Real Money/ Jim Cramer/0743224892", "2/ Jim Cramer's Real Money/ Jim Cramer/0743224892", "2/ Jim Cramer's Real Money/ Jim Cramer/0743224892"};
            

            using (StreamWriter outputFile = new StreamWriter(fileName))
            {
                foreach (String line in example){
                    outputFile.WriteLine(line);

                }
            }
        }
        public List<Book> getPreviousBooks()
        {
            String line = "";

            List<String> data = new List<string>();

            System.IO.StreamReader file = new System.IO.StreamReader(fileName);  

            while ((line = file.ReadLine()) != null) 
            {
                data.Add(line);
                System.Console.WriteLine(line);
            }

            List<Book> books = new List<Book>();

            Book book1 = createBook(parsedData("0/ Jim Cramer's Real Money/ Jim Cramer/0743224892"));
            Book book2 = createBook(parsedData("1/ Jim Cramer's Real Money/ Jim Cramer/0743224892"));
            Book book3 = createBook(parsedData("2/ Jim Cramer's Real Money/ Jim Cramer/0743224892"));

            books.Add(book1);
            books.Add(book2);
            books.Add(book3);


            file.Close();

            return books;
        }
        public List<String> parsedData(String line)
        {
            List<String> split = new List<string>();

            while (line.IndexOf('/') != -1)
            {
                int index = line.IndexOf('/');
                String part = line.Substring(0, index).Trim();
                split.Add(part);

                line = line.Substring(index+1);
            }

            split.Add(line);
            return split;
        }
        public Book createBook(List<String> data)
        {
            return new Book(Convert.ToInt16(data[0]), data[1], data[2], data[3]);
        }
    }
}
