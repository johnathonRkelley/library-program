﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Library_Program
{
    public partial class Description : Form
    {
        private String bookName;
        private String bookISBN;
        private int bookID;
        private String bookAuthor;
        private Boolean changedBookName, changedAuthor, changedISBN;
        private int index; 

        /* Constructor */ 
        public Description(int id, String isbn, String name, String author)
        {
            InitializeComponent();

            bookID = id;
            bookISBN = isbn;
            bookName = name;
            bookAuthor = author;

            this.Text = bookName + " Information";
            
            Database d = new Database();
            index = d.GetIndex(name);
        }
        public Description(int id, String[] data)
        {
            InitializeComponent();

            bookID = id;
            bookName = data[0];
            bookAuthor = data[1];
            bookISBN = data[2];

            this.Text = bookName + " Information";

            changedAuthor = false;
            changedBookName = false;
            changedISBN = false;

        }
        private void Desciption_Load(object sender, EventArgs e)
        {
            //Loads Book Data
            txtAuthor.Text = bookAuthor;
            txtBookName.Text = bookName;
            txtISBN.Text = bookISBN;

            //Default Buttons
            butClose.Text = "Close";
            butSave.Text = "Update";
        }

        /* Button Clicks */ 
        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void butSave_Click(object sender, EventArgs e)
        {
            if (changedISBN == false && changedBookName == false && changedAuthor == false)
            {
                lblChange.Text = "Please change something to save";
            }
            else
            {
                lblChange.Text = "Book Information Saved!";
            }

            Database d = new Database();
            d.Update(bookID, txtBookName.Text, txtAuthor.Text, txtISBN.Text);
            this.Close();
        }

        /* Form Methods */
        private void txtBookName_TextChanged(object sender, EventArgs e)
        {
            if (txtBookName.Text == bookName)
            {
                changedBookName = false;
            }
            else
            {
                changedBookName = true;
            }
        }
        private void txtAuthor_TextChanged(object sender, EventArgs e)
        {
            if (txtAuthor.Text == bookAuthor)
            {
                changedAuthor = false;
            }
            else
            {
                changedAuthor = true;
            }
        }
        private void txtISBN_TextChanged(object sender, EventArgs e)
        {
            if (txtISBN.Text == bookISBN)
            {
                changedISBN = false;
            }
            else
            {
                changedISBN = true;
            }
        }


    }
}
