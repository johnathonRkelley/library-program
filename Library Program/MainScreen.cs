﻿using FireSharp.Config;
using FireSharp.Interfaces;
using FireSharp.Response;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Library_Program
{
    public partial class MainScreen : Form
    {
        //Global Variables
        private int added;
        private String CACHE_FILE;
        private Book[] books;
        private Book3[] books1;
        private int selectedIndex;
        private Label lastLabel;
        private List<Book> saved;
        private List<Book> listOfBooks;
        private List<Book3> currentBooks; //Current Storage
        private List<Book3> saved1; //Temporary Storage
        List<String> list;

        //Initialzation of Form
        public MainScreen()
        {
            InitializeComponent();
            CACHE_FILE = System.IO.Directory.GetCurrentDirectory() + @"\example.txt";
            added = 0;
        }

        //Load of MainScreen Object
        private void MainScreen_Load(object sender, EventArgs e)
        {
            listOfBooks = new List<Book>();
            currentBooks = new List<Book3>();


            books = new Book[300];
            books1 = new Book3[300];

            Label name = new Label();
            Label author = new Label();
            Label isbn = new Label();
            Label number = new Label();

            name.Text = "Book Title";
            author.Text = "Author";
            isbn.Text = "ISBN";
            number.Text = "#";
            //▲ ▼
            name.AutoSize = false;
            name.Dock = DockStyle.Fill;
            name.TextAlign = ContentAlignment.MiddleCenter;

            author.AutoSize = false;
            author.Dock = DockStyle.Fill;
            author.TextAlign = ContentAlignment.MiddleCenter;

            isbn.AutoSize = false;
            isbn.Dock = DockStyle.Fill;
            isbn.TextAlign = ContentAlignment.MiddleCenter;

            number.AutoSize = false;
            number.Dock = DockStyle.Fill;
            number.TextAlign = ContentAlignment.MiddleCenter;

            gridPanel.Controls.Add(number, 0, 0);
            gridPanel.Controls.Add(name, 1, 0);
            gridPanel.Controls.Add(author, 2, 0);
            gridPanel.Controls.Add(isbn, 3, 0);

            Console.WriteLine(CACHE_FILE);
            selectedIndex = 0;

            lastLabel = null;
            Database d = new Database();
            d.Update(3, "New", "New Author Test 3", "No ISBN");
            saved = d.Select();

            FirebaseDB db = new FirebaseDB();
            saved1 = db.getData();

            addPreviousBooks();

            
        }
        //Closing of Form
        private void MainScreen_FormClosing(object sender, FormClosingEventArgs e)
        {
            Backend b = new Backend();
            b.saveData(books);
        }
        /// <summary>
        /// Description : This method is designed to fill table through an insertion (1) and reloading the file (2).
        /// @param : param is required. This will determine where the information is grabbed from
        ///          (1) Insertion from cache file using the timer.
        ///          (2) Insertion through loading up the file.
        /// </summary>
        private void fillTable(int param)
        {
            //Gets the 4 Labels in the table
            Label name = new Label();
            Label author = new Label();
            Label isbn = new Label();
            Label number = new Label();

            //Gets the number of spots used and calculates number for table spot
            int dataSpot = added + 1;
            String a = dataSpot.ToString();

            String[] lines;

            if (param == 1)
            {
                lines = System.IO.File.ReadAllLines(CACHE_FILE);
                System.IO.File.WriteAllBytes(CACHE_FILE, new byte[0]);
                books[dataSpot] = new Book(dataSpot + 1, lines);
                Console.WriteLine(books[dataSpot].savedData());

                //Allocates the information 
                name.Text = lines[0].ToUpper();
                author.Text = lines[1].ToUpper();
                isbn.Text = lines[2].ToUpper();
            }
            else
            {
                books1[dataSpot] = saved1[0];
                currentBooks.Add(saved1[0]);
                //Allocates the information 
                name.Text = saved1[0].bookName;
                author.Text = saved1[0].bookAuthor;
                isbn.Text = saved1[0].isbn;
            }

            name.AutoSize = false;
            name.Dock = DockStyle.Fill;
            name.TextAlign = ContentAlignment.MiddleCenter;
            name.Click += new System.EventHandler(this.selectedItems);
            name.Tag = dataSpot;

            author.AutoSize = false;
            author.Dock = DockStyle.Fill;
            author.TextAlign = ContentAlignment.MiddleCenter;
            author.Click += new System.EventHandler(this.selectedItems);
            author.Tag = dataSpot;

            isbn.AutoSize = false;
            isbn.Dock = DockStyle.Fill;
            isbn.TextAlign = ContentAlignment.MiddleCenter;
            isbn.Click += new System.EventHandler(this.selectedItems);
            isbn.Tag = dataSpot;

            number.AutoSize = false;
            number.Dock = DockStyle.Fill;
            number.TextAlign = ContentAlignment.MiddleCenter;
            number.Text = a;
            number.Click += new System.EventHandler(this.selectedItems);
            number.Tag = dataSpot;
            number.FlatStyle = FlatStyle.Flat;

            //Adds the data to the middlePanel
            middlePanel.Controls.Add(number, 0, added);
            middlePanel.Controls.Add(name, 1, added);
            middlePanel.Controls.Add(author, 2, added);
            middlePanel.Controls.Add(isbn, 3, added);
            middlePanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 40));
            added = added + 1;
        }

        //---- Start Buttons Click Operations ----
        private void butInsert_Click(object sender, EventArgs e)
        {
            new frmAddBook().Show();
            resetTable(null, null);
        }
        private void btnLookup_Click(object sender, EventArgs e)
        {
            new frmIsbnLookup().Show();
            resetTable(null, null);
        }
        private void btnEdit_Click(object sender, EventArgs e)
        {
            Book selectedBook = books[selectedIndex];
            Description d = new Description(selectedBook.getId(), selectedBook.getBookInformation());
            d.FormClosed += new FormClosedEventHandler(resetTable);
            d.Show();
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            int index = selectedIndex - 1;
            Database d = new Database();
            Book selectedBook = listOfBooks[index];

            listOfBooks.RemoveAt(index); //Keeps in bounds.
            Console.WriteLine("Selected Book is " + selectedBook.getId());
            d.Delete(selectedBook.getId());
            resetTable(null, null);
        }

        //---- End Buttons Click Operations ----

        //---- Start Timer Tick Operations ----
        /// <summary>
        /// Timer1 is designed to check for a change in file size when looking to add data from the frmAddBok.
        /// </summary>
        private void timer1_Tick(object sender, EventArgs e)
        {
            string fileName = System.IO.Directory.GetCurrentDirectory() + @"\example.txt";
            FileInfo fi = new FileInfo(CACHE_FILE);

            if (fi.Length != 0)
            {
                fillTable(1);
            }
        }

        //---- End Timer Tick Operations ----
        
        //---- Start Key Methods -----
        /// <summary>
        /// selectedItems method
        /// </summary>8mo     
        private void selectedItems(object sender, EventArgs e)
        {
            Label sent = sender as Label;
            int index = (int)sent.Tag;

            if (lastLabel == null) //Not clicked
            {
                //sent.BackColor = System.Drawing.Color.Aqua;
                sent.BackColor = System.Drawing.Color.LightGray;
                lastLabel = sent;
            }
            else // If label selected previously...
            {
                lastLabel.BackColor = System.Drawing.Color.White;
                lastLabel = sent;
                sent.BackColor = System.Drawing.Color.LightGray;
            }
            selectedIndex = index;
            Console.WriteLine(selectedIndex);
        }
        private void addPreviousBooks()
        {
            while (saved1.Count() > 0) //Makes sure there are books
            {
                fillTable(2); //Fills table with said books
                saved1.RemoveAt(0); //Remove first index
            }
        }
        private void updateTable()
        {
            added = 0;
            Database d = new Database();

            middlePanel.Visible = false;
            middlePanel.Controls.Clear();
            middlePanel.Visible = true;

            saved = d.Select();
            FirebaseDB db = new FirebaseDB();

            saved1 = db.getData();
            addPreviousBooks();
        }
        public void resetTable(object sender, FormClosedEventArgs e)
        {
            Console.WriteLine("Form was closed");
            updateTable();
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            FirebaseDB db = new FirebaseDB();
            List<Book3> booksFromDB = db.getData();

            compareBookLists(booksFromDB);
        }

        private void compareBookLists(List<Book3> newBookList)
        {
            List<Book3> toBeAdded = new List<Book3>();

            if (newBookList.Count != currentBooks.Count) // If arent the same
            {
                for (int i = 0; i < newBookList.Count; i++)
                {
                    Book3 newBook = newBookList[i];

                    Boolean isNewBook = true;
                    for (int j = 0; j < currentBooks.Count; j++)
                    {
                        Book3 oldBooks = currentBooks[j];

                        if (oldBooks.isbn == newBook.isbn)
                        {
                            isNewBook = false;
                        }
                    }

                    if (isNewBook)
                    {
                        toBeAdded.Add(newBook);
                    }
                }
            }

            //Add all books to current
            foreach (Book3 book in toBeAdded)
            {
                saved1.Add(book);
            }

            updateTable();
        }
    }
}