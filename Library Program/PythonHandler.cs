﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Collections;

namespace Library_Program
{
    class PythonHandler
    {
        private String currentDirectory;
        public PythonHandler()
        {
            currentDirectory = System.IO.Directory.GetCurrentDirectory() + @"\py\";  
        }

        public List<String> run_cmd(string isbn)
        {
            Process p = new Process(); // create process to run the python program
                      
            string[] scripts = {"isbnLookup.py"};

            //Gets File Directory for desired script
            string fileName = currentDirectory + scripts[0];
            Console.WriteLine(fileName);

            //Process Start Info
            p.StartInfo.FileName = "python.exe"; //Python.exe location
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.UseShellExecute = false; // ensures you can read stdout
            p.StartInfo.CreateNoWindow = true;
            p.StartInfo.Arguments = string.Format("\"{0}\"", fileName) + " " + isbn;
            p.Start();

            StreamReader s = p.StandardOutput;

            String line = "";

            List<String> list = new List<String>();

            while ((line = s.ReadLine()) != null)
            {
                list.Add(line);
                Console.WriteLine(line);
            }

            list.Add(price_grabber(isbn));

            p.WaitForExit();
            return list;
        }


        public String price_grabber(string parameter1)
        {
            Process p = new Process(); // create process to run the python program
            string[] scripts = {"priceChecker.py"};

            //Gets File Directory for desired script
            string fileName = currentDirectory + scripts[0];

            //Process Start Info
            p.StartInfo.FileName = "python.exe"; //Python.exe location
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.UseShellExecute = false; // ensures you can read stdout
            p.StartInfo.CreateNoWindow = true;
            p.StartInfo.Arguments = string.Format("\"{0}\"", fileName) + " " + parameter1;
            p.Start();

            StreamReader s = p.StandardOutput;
            String data = s.ReadToEnd();
            Console.WriteLine(data);
            p.WaitForExit();
               
            return data;
        }
    }
}
